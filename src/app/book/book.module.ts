import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookComponent } from './book.component';
import { BookFilterPipe } from './pipes/book-filter.pipe';
import { BookCardComponent } from './components/book-card/book-card.component';
import { BookDetailComponent } from './components/book-detail/book-detail.component';
import { BookRoutingModule } from './book-routing.module';
import { BookApiService } from './services/book-api.service';
import { BookEditComponent } from './components/book-edit/book-edit.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    BookComponent,
    BookFilterPipe,
    BookCardComponent,
    BookDetailComponent,
    BookEditComponent,
  ],
  imports: [CommonModule, BookRoutingModule, FormsModule],
  exports: [BookComponent],
  providers: [BookApiService]
})
export class BookModule {}
