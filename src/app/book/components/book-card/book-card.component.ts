import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Book } from '../../models/book';

@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.scss'],
})
export class BookCardComponent implements OnInit {

  @Input()
  content: Book | undefined

  @Output()
  detailClick = new EventEmitter<Book>();

  customStyles = {
    // color: 'red'
  }

  constructor() { }

  ngOnInit(): void {
  }

  handleDetailClick(event: MouseEvent) {
    event.preventDefault();
    console.log('Go to Detail', event)
    this.detailClick.emit(this.content);
  }

}
