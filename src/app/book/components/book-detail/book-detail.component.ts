import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EMPTY, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Book } from '../../models/book';
import { BookApiService } from '../../services/book-api.service';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.scss'],
})
export class BookDetailComponent implements OnInit {
  book$: Observable<Book> = EMPTY;

  constructor(private route: ActivatedRoute, private bookApi: BookApiService) {}

  ngOnInit(): void {
    this.book$ = this.route.params.pipe(
      switchMap((params) => {
        const bookIsbn: string = params.isbn;
        const book$ = this.bookApi.getByIsbn(bookIsbn);
        return book$;
      })
    );
  }
}
