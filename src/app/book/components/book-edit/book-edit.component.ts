import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, EMPTY } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Book } from '../../models/book';
import { BookApiService } from '../../services/book-api.service';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.scss']
})
export class BookEditComponent implements OnInit {

  book: Book | undefined

  constructor(private route: ActivatedRoute, private bookApi: BookApiService) {}

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap((params) => {
        const bookIsbn: string = params.isbn;
        const book$ = this.bookApi.getByIsbn(bookIsbn);
        return book$;
      })
    ).subscribe(bookFromApi => this.book = bookFromApi)
  }

  save(value: Book) {
    console.log({value, book: this.book});
  }

}
