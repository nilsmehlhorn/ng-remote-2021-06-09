import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BookModule } from '../book.module';
import { Book } from '../models/book';

@Injectable()
export class BookApiService {
  getAll(): Observable<Book[]> {
    return this.http.get<Book[]>('http://localhost:4730/books');
  }

  getByIsbn(isbn: string): Observable<Book> {
    return this.http.get<Book>(`http://localhost:4730/books/${isbn}`)
  }

  constructor(private http: HttpClient) {}
}
