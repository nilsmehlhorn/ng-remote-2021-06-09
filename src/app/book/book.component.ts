import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EMPTY, Observable, Subscription } from 'rxjs';
import { Book } from './models/book';
import { BookApiService } from './services/book-api.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss'],
})
export class BookComponent implements OnInit {
  books$: Observable<Book[]> = EMPTY;

  bookSearchTerm = '';

  constructor(private bookApi: BookApiService, private router: Router) {}

  ngOnInit(): void {
    this.books$ = this.bookApi.getAll();
  }

  goToBookDetails({isbn}: Book): void {
    console.log('Navigate to book details now', isbn);
    this.router.navigate(['/books', 'details', isbn])
  }

  updateBookSearchTerm(event: Event): void {
    this.bookSearchTerm = (event.target as HTMLInputElement).value;
  }
}
