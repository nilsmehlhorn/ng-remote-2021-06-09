import { Pipe, PipeTransform } from '@angular/core';
import { Book } from '../models/book';

@Pipe({
  name: 'bookFilter',
})
export class BookFilterPipe implements PipeTransform {
  transform(books: Book[] | null, searchTerm: string = ''): Book[] {
    if (!books) {
      return [];
    }
    const loweredSearchTerm = searchTerm.toLowerCase();
    return books.filter(({title, abstract}) => {
      return (
        title.toLowerCase().includes(loweredSearchTerm) ||
        abstract.toLowerCase().includes(loweredSearchTerm)
      );
    });
  }
}
